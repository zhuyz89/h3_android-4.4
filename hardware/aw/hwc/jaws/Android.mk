# Copyright (C) 2008 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


LOCAL_PATH := $(call my-dir)

# HAL module implemenation stored in
# hw/<OVERLAY_HARDWARE_MODULE_ID>.<ro.product.board>.so
include $(CLEAR_VARS)

LOCAL_MODULE_PATH := $(TARGET_OUT_SHARED_LIBRARIES)/hw
LOCAL_SHARED_LIBRARIES := liblog libEGL
LOCAL_SRC_FILES := hwc.cpp \
									 hwc_sunxi.cpp \
									 hwc_others.cpp \
									 hal.cpp
LOCAL_SHARED_LIBRARIES := \
	libutils \
	libEGL \
	libGLESv1_CM \
	liblog \
	libcutils \
	libedidParse.sunxi

LOCAL_C_INCLUDES += $(TARGET_HARDWARE_INCLUDE)
LOCAL_C_INCLUDES +=system/core/include/ \
	hardware/aw/edidParse
LOCAL_MODULE := hwcomposer.$(TARGET_BOARD_PLATFORM)
LOCAL_CFLAGS:= -DLOG_TAG=\"hwcomposer\"

ifneq ($(HDMI_CHANNEL),)
    LOCAL_CFLAGS += -DHDMI_CHANNEL=$(HDMI_CHANNEL)
else
    LOCAL_CFLAGS += -DHDMI_CHANNEL=-1
endif
ifneq ($(HDMI_DEFAULT_MODE),)
    LOCAL_CFLAGS += -DDISP_DEFAULT_HDMI_MODE=$(HDMI_DEFAULT_MODE)
else
    LOCAL_CFLAGS += -DDISP_DEFAULT_HDMI_MODE=4
endif

ifneq ($(CVBS_CHANNEL),)
    LOCAL_CFLAGS += -DCVBS_CHANNEL=$(CVBS_CHANNEL)
else
    LOCAL_CFLAGS += -DCVBS_CHANNEL=-1
endif
ifneq ($(CVBS_DEFAULT_MODE),)
    LOCAL_CFLAGS += -DDISP_DEFAULT_CVBS_MODE=$(CVBS_DEFAULT_MODE)
else
    LOCAL_CFLAGS += -DDISP_DEFAULT_CVBS_MODE=11
endif

LOCAL_MODULE_TAGS := optional
include $(BUILD_SHARED_LIBRARY)
