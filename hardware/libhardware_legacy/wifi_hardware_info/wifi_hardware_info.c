/*
 * Copyright 2008, The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <stdlib.h>
#include <fcntl.h>
#include <errno.h>
#include <string.h>
#include <dirent.h>
#include <sys/socket.h>
#include <unistd.h>
#include <poll.h>
#include <hardware_legacy/uevent.h>
#include <sys/socket.h>
#include <linux/netlink.h>
#include <poll.h>

#include "hardware_legacy/wifi.h"
#define LOG_TAG "WifiHWInfo"
#include "cutils/log.h"
#include "cutils/properties.h"

#define CMDLINE_PATH               "/proc/cmdline"
#define MAC_KEY_VALUE              "wifi_mac"
#define RTW_MAC_SETVALUE           "rtw_initmac="
#define TRY_COUNT                  3

#define SDIO_VENDOR_ID_BROADCOM         "02D0"
#define SDIO_DEVICE_ID_BROADCOM_DEFAULT "0000"
#define SDIO_DEVICE_ID_BROADCOM_6210    "A962"
#define SDIO_DEVICE_ID_BROADCOM_6335    "4335"
#define SDIO_DEVICE_ID_BROADCOM_6330    "4330"

#define SDIO_VENDOR_ID_REALTEK          "024C"
#define SDIO_DEVICE_ID_REALTEK_8189     "8179"
#define SDIO_DEVICE_ID_REALTEK_8723     "B723"

#define USB_VENDOR_ID_REALTEK           0x0BDA
#define USB_DEVICE_ID_REALTEK_8188EU    0x0179
#define USB_DEVICE_ID_REALTEK_8188ETV   0x8179

static int saved_wifi_hardware_info = 0;
static int ls_device_thread_started = 0;
static int get_wifi_hardware_info_failed = 0;

static int get_mac_address(char *name, char *mac_value)
{
    char *value = strchr(name, '=');
    if(value == 0){
        return -1;
    }
    *value++ = 0;
    int name_len = strlen(name);
    if(name_len == 0){
        return -1;
    }
    if(!strcmp(MAC_KEY_VALUE, name)){
        strcpy(mac_value, value);
        return 0;
    }
    return -1;
}
void parse_cmdline(char* value)
{
    char cmdline[1024];
    char *ptr = NULL;
    int fd = -1;

    fd = open(CMDLINE_PATH, O_RDONLY);
    if(fd >= 0){
        int n = read(fd, cmdline, 1023);
        if(n < 0){
            n = 0;
        }
        if(n > 0 && cmdline[n-1] == '\n'){
            n--;
        }
        cmdline[n] = 0;
        close(fd);
    }else{
        cmdline[0] = 0;
        ALOGD("read cmdline fail,");
    }
    ptr = cmdline;
    while (ptr && *ptr) {
        char *x = strchr(ptr, ' ');
        if(x != 0){
            *x++ = 0;
        }
        if(get_mac_address(ptr, value) == 0)
            return;
        ptr = x;
    }
}

void get_driver_module_arg(char* arg) 
{
    char module_arg[256] = {0};
    char mac[18] = {0};

    parse_cmdline(mac);
    if(strncmp(get_wifi_vendor_name(), "realtek", strlen("realtek")) == 0) {
        const char *driver_module_arg = "ifname=wlan0 if2name=p2p0";
        if(mac != NULL && *mac != 0) {
            snprintf(module_arg, sizeof(module_arg), "%s %s%s", driver_module_arg, RTW_MAC_SETVALUE, mac);
        } else {
            snprintf(module_arg, sizeof(module_arg), "%s", driver_module_arg);
        }
    } else if(strncmp(get_wifi_vendor_name(), "broadcom", strlen("broadcom")) == 0) {
        const char *driver_module_arg = "nvram_path=/system/vendor/modules/nvram_";
        snprintf(module_arg, sizeof(module_arg), "%s%s.txt", driver_module_arg, get_wifi_module_name());
    }
    strcpy(arg, module_arg);
}

static void parse_uevent(char *msg)
{
    char sdio_device[10] = {0};
    char sdio_vendor[10] = {0};

    int usb_pid = 0;
    int usb_vid = 0;
    char device_type[10] = {0};
    char wifi_vendor_name[10] = {0};
    char wifi_module_name[10] = {0};
    char *subsystem = "";
    char *sdio_id = "";
    char *usb_product = "";

    while(*msg) {
        if(!strncmp(msg, "SUBSYSTEM=", 10)) {
            msg += 10;
            subsystem = msg;
        } else if(!strncmp(msg, "SDIO_ID=", 8)) {
            msg += 8;
            sdio_id = msg;
        } else if(!strncmp(msg, "PRODUCT=", 8)) {
            msg += 8;
            usb_product = msg;
        }

        /* advance to after the next \0 */
        while(*msg++)
            ;
    }
    if(!strncmp(subsystem, "sdio", 4)) {
        strcpy(device_type, "sdio");
        char *sdio_device_id = strrchr(sdio_id, ':');
        if(sdio_device_id == 0) {
            return;
        }
        sdio_device_id++;
        strcpy(sdio_device, sdio_device_id);
        *sdio_device_id = 0;
        strcpy(sdio_vendor, sdio_id);
        if(strncmp(sdio_vendor, SDIO_VENDOR_ID_REALTEK,
                strlen(SDIO_VENDOR_ID_REALTEK)) == 0) {
            strcpy(wifi_vendor_name, "realtek");
            ALOGI("ls_device: get realtek sdio wifi!!!");
        } else if(strncmp(sdio_vendor, SDIO_VENDOR_ID_BROADCOM,
                strlen(SDIO_VENDOR_ID_BROADCOM)) == 0){
            strcpy(wifi_vendor_name, "broadcom");
            ALOGI("ls_device: get broadcom sdio wifi!!!");
        } else {
            strcpy(wifi_vendor_name, "unknow");
        }

        if(strncmp(wifi_vendor_name, "unknow", strlen("unknow")) != 0) {
            if(strncmp(sdio_device, SDIO_DEVICE_ID_REALTEK_8189,
                    strlen(SDIO_DEVICE_ID_REALTEK_8189)) == 0) {
                strcpy(wifi_module_name, "8189es");
            } else if(strncmp(sdio_device, SDIO_DEVICE_ID_REALTEK_8723,
                    strlen(SDIO_DEVICE_ID_REALTEK_8723)) == 0) {
                strcpy(wifi_module_name, "8723bs");
            } else if(strncmp(sdio_device, SDIO_DEVICE_ID_BROADCOM_6210,
                     strlen(SDIO_DEVICE_ID_BROADCOM_6210)) == 0) {
                strcpy(wifi_module_name, "ap6210");
            } else if(strncmp(sdio_device, SDIO_DEVICE_ID_BROADCOM_6330,
                     strlen(SDIO_DEVICE_ID_BROADCOM_6330)) == 0) {
                strcpy(wifi_module_name, "ap6330");
            } else if(strncmp(sdio_device, SDIO_DEVICE_ID_BROADCOM_6335,
                     strlen(SDIO_DEVICE_ID_BROADCOM_6335)) == 0) {
                strcpy(wifi_module_name, "ap6335");
            } else {
                strcpy(wifi_module_name, "unknow");
            }
        }
    } else if (!strncmp(subsystem, "usb", 3)) {
        strcpy(device_type, "usb");
        char *result = NULL;
        int i = 0;
        result = strtok( usb_product, "/");
        while( result != NULL && i < 2) {
            switch(i) {
                case 0:
                    usb_vid = strtol(result, NULL, 16);
                    break;
                case 1:
                    usb_pid = strtol(result, NULL, 16);
                    break;
                default:
                    break;
            }
            result = strtok( NULL, "/");
            i += 1;
        }
        if(usb_vid == USB_VENDOR_ID_REALTEK) {
            strcpy(wifi_vendor_name, "realtek");
            ALOGI("ls_device: get realtek usb wifi !!!");
        } else {
            strcpy(wifi_vendor_name, "unknow");
        }

        if(strncmp(wifi_vendor_name, "unknow", strlen("unknow")) != 0) {
            if(usb_pid == USB_DEVICE_ID_REALTEK_8188EU) {
                strcpy(wifi_module_name, "8188eu");
            } else if(usb_pid == USB_DEVICE_ID_REALTEK_8188ETV) {
                strcpy(wifi_module_name, "8188eu");
            } else {
                strcpy(wifi_module_name, "unknow");
            }
        }
    } else {
        strcpy(device_type, "unknow");
        strcpy(wifi_vendor_name, "unknow");
        strcpy(wifi_module_name, "unknow");
    }
    if(strncmp(wifi_vendor_name, "unknow", 6) &&  strncmp(wifi_module_name, "unknow", 6)) {
        ALOGD("write wifi_hardware_info into file: %s:%s:%s", device_type, wifi_vendor_name, wifi_module_name);
        FILE *file = fopen("/data/wifi_hardware_info", "w");
        if(file == NULL){
            ALOGE("cannot open file /data/wifi_hardware_info to write");
            return;
        }
        fprintf(file,"%s:%s:%s", device_type, wifi_vendor_name, wifi_module_name);
        fflush(file);
        fsync(fileno(file));
        fclose(file);
        saved_wifi_hardware_info = 1;
    }
}

#define UEVENT_MSG_LEN  1024
static int ls_device_thread()
{
    char buf[UEVENT_MSG_LEN + 2] = {0};
    int count;
    int err;
    int retval;
    struct sockaddr_nl snl;
    int sock;
    struct pollfd fds;
    const int buffersize = 32*1024;

	memset(&snl, 0x0, sizeof(snl));
	snl.nl_family = AF_NETLINK;
	snl.nl_pid = 0;
	snl.nl_groups = 0xffffffff;
	sock = socket(PF_NETLINK, SOCK_DGRAM, NETLINK_KOBJECT_UEVENT);
	setsockopt(sock, SOL_SOCKET, SO_RCVBUFFORCE, &buffersize, sizeof(buffersize));
	retval = bind(sock, (struct sockaddr *)&snl, sizeof(struct sockaddr_nl));
	if (sock == -1) {
		ALOGE("####socket is failed in %s error:%d %s###", __FUNCTION__, errno, strerror(errno));
		return -1;
	}
    ls_device_thread_started = 1;
    while (!saved_wifi_hardware_info) {
        fds.fd = sock;
        fds.events = POLLIN;
        fds.revents = 0;
        err = poll(&fds, 1, 1000);
        memset(buf, '\0', sizeof(char) * 1024);
        if(err > 0 && (fds.revents & POLLIN)) {
           count = recv(sock, buf, sizeof(char) * 1024,0);
           if(count > 0) {
                parse_uevent(buf);
            }
        }
    }
    return 0;
}
int get_wifi_hardware_info(char* wifi_hardware_info)
{
	int fd = -1;
	int fd_power = -1;
	pthread_t fd_ls_device_thread;
	int size = 0;
	int ret = 0;
	char buffer = '0';

	property_get("wlan.hardware.info", wifi_hardware_info, "");
	if(wifi_hardware_info == NULL || strlen(wifi_hardware_info) == 0) {
	    if (get_wifi_hardware_info_failed || access("/data/wifi_hardware_info", 0) == -1) {
	    	saved_wifi_hardware_info = 0;
			ALOGD("get hardware info from /data/wifi_hardware_info failed, try to create it!");
			ret = pthread_create(&fd_ls_device_thread, NULL,(void *) ls_device_thread, NULL);
			if( ret!=0 ) {
				ALOGE("Create ls_device_thread error!\n");
				return -1;
			}
			while(!ls_device_thread_started)
				usleep(100);
		    fd_power = open("/proc/driver/wifi-pm/power", O_WRONLY);
		    if (fd_power < 0) {
		        ALOGE("power on wifi module : open(/proc/driver/wifi-pm/power) failed: %s (%d)",
		            strerror(errno), errno);
		        pthread_join(fd_ls_device_thread, NULL);
				ls_device_thread_started = 0;
		        return -1;
		    }
		    buffer = '1';
		    size = write(fd_power, &buffer, 1);
		    if (size < 0) {
		        ALOGE("power on wifi module : write(/proc/driver/wifi-pm/power) failed: %s (%d)",
		            strerror(errno),errno);
		        close(fd_power);
		        pthread_join(fd_ls_device_thread, NULL);
				ls_device_thread_started = 0;
		        return -1;
		    }
		    usleep(500000);
			buffer = '0';
		    size = write(fd_power, &buffer, 1);
		    if (size < 0) {
		        ALOGE("power off wifi module : write(/proc/driver/wifi-pm/power) failed: %s (%d)",
		            strerror(errno),errno);
		        close(fd_power);
		        pthread_join(fd_ls_device_thread, NULL);
				ls_device_thread_started = 0;
		        return -1;
		    }
		    usleep(500000);
	        close(fd_power);
	        pthread_join(fd_ls_device_thread, NULL);
			ls_device_thread_started = 0;
	    } else {
	    	saved_wifi_hardware_info = 1;
	    }
	    if(access("/data/wifi_hardware_info", 0) == 0) {
		    fd = TEMP_FAILURE_RETRY(open("/data/wifi_hardware_info", O_RDONLY));
		    if (fd < 0) {
		        ALOGE("Failed to open wifi_hardware_info (%s)", strerror(errno));
		        return -1;
		    }
		    if (TEMP_FAILURE_RETRY(read(fd, wifi_hardware_info, 128)) < 0) {
		        ALOGE("Failed to read wifi_hardware_info (%s)", strerror(errno));
		        close(fd);
		        return -1;
		    }
		    close(fd);
		    property_set("wlan.hardware.info", wifi_hardware_info);
		    return 0;
	    } else {
			return -1;
	    }
	} else {
		return 0;
	}
}

/* return "realtek" "broadcom" "unknow" */
const char *get_wifi_vendor_name()
{
#if defined(WIFI_VENDOR_NAME)
	return WIFI_VENDOR_NAME;
#else
	int i = 0;
	char wifi_hardware_info[56];
	for(i = 0; i < TRY_COUNT; i++) {
		memset(wifi_hardware_info, 0, sizeof(wifi_hardware_info));
		get_wifi_hardware_info(wifi_hardware_info);
		get_wifi_hardware_info_failed = 0;
		if(strstr(wifi_hardware_info, "realtek") != NULL) {
			return "realtek";
		} else if(strstr(wifi_hardware_info, "broadcom") != NULL) {
			return "broadcom";
		} else {
			get_wifi_hardware_info_failed = 1;
			property_set("wlan.hardware.info", "");
		}
	}
	ALOGE("get_wifi_vendor_name failed, return defalut value: realtek");
	return "realtek";
#endif
}

/* return "8188eu" "8189es" "8723bs" "ap6210" "ap6330" "ap6335" "unknow" */
const char *get_wifi_module_name()
{
#if defined(WIFI_MODULE_NAME)
	return WIFI_MODULE_NAME;
#else
	int i = 0;
	char wifi_hardware_info[56];
	for(i = 0; i < TRY_COUNT; i++) {
		memset(wifi_hardware_info, 0, sizeof(wifi_hardware_info));
		get_wifi_hardware_info(wifi_hardware_info);
		get_wifi_hardware_info_failed = 0;
		if(strstr(wifi_hardware_info, "8188eu") != NULL) {
			return "8188eu";
		} else if(strstr(wifi_hardware_info, "8189es") != NULL) {
			return "8189es";
		} else if(strstr(wifi_hardware_info, "8723bs") != NULL) {
			return "8723bs";
		} else if(strstr(wifi_hardware_info, "ap6210") != NULL) {
			return "ap6210";
		} else if(strstr(wifi_hardware_info, "ap6330") != NULL) {
			return "ap6330";
		} else if(strstr(wifi_hardware_info, "ap6335") != NULL) {
			return "ap6335";
		} else {
			get_wifi_hardware_info_failed = 1;
			property_set("wlan.hardware.info", "");
		}
	}
	ALOGE("get_wifi_module_name failed, return defalut value: 8188eu");
	return "8188eu";
#endif
}

/* return "8188eu" "8189es" "8723bs" "bcmdhd" "unknow" */
const char *get_wifi_driver_name()
{
#if defined(WIFI_DRIVER_NAME)
	return WIFI_DRIVER_NAME;
#else
	const char *wifi_module_name = get_wifi_module_name();
	if(!strncmp(wifi_module_name, "ap6210", strlen("ap6210"))) {
		return "bcmdhd";
	} else if(!strncmp(wifi_module_name, "ap6330", strlen("ap6330"))) {
		return "bcmdhd";
	} else if(!strncmp(wifi_module_name, "ap6335", strlen("ap6335"))) {
		return "bcmdhd";
	} else if(strncmp(wifi_module_name, "8188eu", strlen("8188eu")) == 0) {
		return "8188eu";
	} else if(strncmp(wifi_module_name, "8189es", strlen("8189es")) == 0) {
		return "8189es";
	} else if(strncmp(wifi_module_name, "8723bs", strlen("8723bs")) == 0) {
		return "8723bs-vq0";
	} else {
		ALOGE("get_wifi_driver_name failed, return defalut value: 8188eu");
		return "8188eu";
	}
#endif
}
