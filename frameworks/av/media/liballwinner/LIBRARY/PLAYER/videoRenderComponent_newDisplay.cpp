
//#define CONFIG_LOG_LEVEL    OPTION_LOG_LEVEL_DETAIL

#define LOG_TAG "videoRenderComponent_newDisplay"
#include "log.h"

#include <pthread.h>
#include <semaphore.h>
#include <malloc.h>
#include <memory.h>
#include <time.h>

#include "videoRenderComponent.h"
#include "messageQueue.h"
#include "layerControl_newDisplay.h"
#include "memoryAdapter.h"
#include <deinterlace.h>

#include<sys/types.h>  
#include<sys/stat.h>
#include<fcntl.h>
#include <linux/ioctl.h>

#define USE_DETNTERLACE 1

#define MAX_VIDEO_BUFFER_NUM 32

#define VE_PHYADDR_ALIGN 1024

typedef struct VideoBufferInfoS
{
    VideoPicture mVideoPicture;
    int   bUsedFlag;;
    int   bDisplayedFlag;
    int   bCanReturnFlag;
    int   nReturnCount;
}VideoBufferInfoT;

typedef struct VDisplayBufferNodeS VDisplayBufferNodeT; 

struct VDisplayBufferNodeS
{
    VideoBufferInfoT*       pVideoBufferInfo;
    VDisplayBufferNodeT*    pNext;
    int                     bUseFlag;
};

typedef struct VideoRenderCompContext
{
    //* created at initialize time.
    MessageQueue*        mq;
    sem_t                startMsgReplySem;
    sem_t                stopMsgReplySem;
    sem_t                pauseMsgReplySem;
    sem_t                resetMsgReplySem;
    sem_t                eosMsgReplySem;
    sem_t                quitMsgReplySem;
    sem_t                set3DModeReplySem;
    sem_t                setWindowReplySem;
    sem_t                setHideVideoSem;
    sem_t                setHoldLastPictureSem;
    
    int                  nStartReply;
    int                  nStopReply;
    int                  nPauseReply;
    int                  nResetReply;
    int                  nSet3DModeReply;
    
    pthread_t            sRenderThread;
    
    enum EPLAYERSTATUS   eStatus;
    void*                pNativeWindow;
    LayerCtrl*           pLayerCtrl;
    VideoDecComp*        pDecComp;
    
    enum EPICTURE3DMODE  ePicture3DMode;
    enum EDISPLAY3DMODE  eDisplay3DMode;
    
    //* objects set by user.
    AvTimer*             pAvTimer;
    PlayerCallback       callback;
    void*                pUserData;
    int                  bEosFlag;
	int                  nRotationAngle;
    VideoPicture*        pDeinterlacePrePicture;
    
    //*
    VideoBufferInfoT     mVideoBufferInfo[MAX_VIDEO_BUFFER_NUM];
    VDisplayBufferNodeT* pDisplayerBufferListHead;
    VDisplayBufferNodeT  displayBufferNode[MAX_VIDEO_BUFFER_NUM];
    pthread_mutex_t      videoBufferInfoMutex;
    pthread_mutex_t      layerCtrlMutex;
    int                  bResolutionChange;

    int                  bHadSetLayerInfoFlag;
    int                  nGpuAlign;
    int                  bProtectedBufferFlag;//* 1: mean the video picture is secure

    //* for 3D video stream
    VideoPicture         mSecondStreamPicBufInfo;
    int                  bIsSecondStreamFlag;
    int                  nVideoBufferWidth;
    int                  nVideoBufferHeight;
    int                  bVideoWithTwoStream;

    //* for deinterlace
    Deinterlace         *di;
    int                  bDeinterlaceFlag;
    int                  nVideoBufferCount;
    int                  nBufferOwnedByDecoderNum;

    FbmBufInfo           mFbmBufInfo;

}VideoRenderCompContext;

static void* VideoRenderThread(void* arg);
static void PostRenderMessage(MessageQueue* mq);
static int IsVideoWithTwoStream(VideoDecComp* pDecComp);
static VideoBufferInfoT* RequestDisplayPicture(VideoRenderCompContext* p);
static void ReturnBufferToGpu(VideoRenderCompContext* p);
static int VideoRenderCompCallback(void* pUserData, int eMessageId, void* param);

VideoRenderComp* VideoRenderCompCreate(void)
{
    VideoRenderCompContext* p;
    int                     err;
    
    p = (VideoRenderCompContext*)malloc(sizeof(VideoRenderCompContext));
    if(p == NULL)
    {
        loge("memory alloc fail.");
        return NULL;
    }
    memset(p, 0, sizeof(*p));

    p->bVideoWithTwoStream = -1;
    p->mq = MessageQueueCreate(4, "VideoRenderMq");
    if(p->mq == NULL)
    {
        loge("video render component create message queue fail.");
        free(p);
        return NULL;
    }
    
    sem_init(&p->startMsgReplySem, 0, 0);
    sem_init(&p->stopMsgReplySem, 0, 0);
    sem_init(&p->pauseMsgReplySem, 0, 0);
    sem_init(&p->resetMsgReplySem, 0, 0);
    sem_init(&p->eosMsgReplySem, 0, 0);
    sem_init(&p->quitMsgReplySem, 0, 0);
    sem_init(&p->set3DModeReplySem, 0, 0);
    sem_init(&p->setWindowReplySem, 0, 0);
    sem_init(&p->setHideVideoSem, 0, 0);
    sem_init(&p->setHoldLastPictureSem, 0, 0);
    
    p->eStatus = PLAYER_STATUS_STOPPED;

    p->di = DeinterlaceCreate();
    if (!p->di)
    {
        logw("No deinterlace...");
    }
    
    err = pthread_create(&p->sRenderThread, NULL, VideoRenderThread, p);
    if(err != 0)
    {
        loge("video render component create thread fail.");
        sem_destroy(&p->startMsgReplySem);
        sem_destroy(&p->stopMsgReplySem);
        sem_destroy(&p->pauseMsgReplySem);
        sem_destroy(&p->resetMsgReplySem);
        sem_destroy(&p->eosMsgReplySem);
        sem_destroy(&p->quitMsgReplySem);
        sem_destroy(&p->set3DModeReplySem);
        sem_destroy(&p->setWindowReplySem);
        sem_destroy(&p->setHideVideoSem);
        sem_destroy(&p->setHoldLastPictureSem);
        MessageQueueDestroy(p->mq);
        if (p->di)
        {
            delete p->di;
    		p->di = NULL;
		}
        free(p);
        return NULL;
    }

    err = pthread_mutex_init(&p->videoBufferInfoMutex, NULL);
    err = err | pthread_mutex_init(&p->layerCtrlMutex, NULL);
	if(err != 0)
	{
        loge("(f:%s, l:%d) pthread_mutex_init fail!", __FUNCTION__, __LINE__);
        sem_destroy(&p->startMsgReplySem);
        sem_destroy(&p->stopMsgReplySem);
        sem_destroy(&p->pauseMsgReplySem);
        sem_destroy(&p->resetMsgReplySem);
        sem_destroy(&p->eosMsgReplySem);
        sem_destroy(&p->quitMsgReplySem);
        sem_destroy(&p->set3DModeReplySem);
        sem_destroy(&p->setWindowReplySem);
        sem_destroy(&p->setHideVideoSem);
        sem_destroy(&p->setHoldLastPictureSem);
        MessageQueueDestroy(p->mq);
        free(p);
        return NULL;
	}

    //* now on chip-1673, the gpu align is 32 ,other chip is 16.
#if(CONFIG_CHIP == OPTION_CHIP_1673)  
    p->nGpuAlign = 16;
#else
    p->nGpuAlign = 16;
#endif
    
    return (VideoRenderComp*)p;
}


int VideoRenderCompDestroy(VideoRenderComp* v)
{
    logd("VideoRenderCompDestroy!!!");
    void*                   status;
    VideoRenderCompContext* p;
    Message                 msg;
    
    p = (VideoRenderCompContext*)v;

    VideoDecCompSetVideoRenderCallback(p->pDecComp, NULL, NULL);
    
    msg.messageId = MESSAGE_ID_QUIT;
    msg.params[0] = (unsigned int)&p->quitMsgReplySem;
    msg.params[1] = msg.params[2] = msg.params[3] = 0;
    
    if(MessageQueuePostMessage(p->mq, &msg) != 0)
    {
        loge("fatal error, video render component post message fail.");
        abort();
    }
    
    SemTimedWait(&p->quitMsgReplySem, -1);
    pthread_join(p->sRenderThread, &status);
    
    sem_destroy(&p->startMsgReplySem);
    sem_destroy(&p->stopMsgReplySem);
    sem_destroy(&p->pauseMsgReplySem);
    sem_destroy(&p->resetMsgReplySem);
    sem_destroy(&p->eosMsgReplySem);
    sem_destroy(&p->quitMsgReplySem);
    sem_destroy(&p->set3DModeReplySem);
    sem_destroy(&p->setWindowReplySem);
    sem_destroy(&p->setHideVideoSem);
    sem_destroy(&p->setHoldLastPictureSem);

    pthread_mutex_destroy(&p->videoBufferInfoMutex);
    pthread_mutex_destroy(&p->layerCtrlMutex);
    
    if(p->pDeinterlacePrePicture != NULL)
        VideoDecCompReturnPicture(p->pDecComp, p->pDeinterlacePrePicture);
    
    if (p->di)
    {
        delete p->di;
        p->di = NULL;
    }
    MessageQueueDestroy(p->mq);
    free(p);
    
    return 0;   
}


int VideoRenderCompStart(VideoRenderComp* v)
{
    VideoRenderCompContext* p;
    Message                 msg;
    
    p = (VideoRenderCompContext*)v;
    
    logv("video render component starting");
    
    msg.messageId = MESSAGE_ID_START;
    msg.params[0] = (unsigned int)&p->startMsgReplySem;
    msg.params[1] = (unsigned int)&p->nStartReply;
    msg.params[2] = msg.params[3] = 0;
    
    if(MessageQueuePostMessage(p->mq, &msg) != 0)
    {
        loge("fatal error, video render component post message fail.");
        abort();
    }
    
    if(SemTimedWait(&p->startMsgReplySem, -1) < 0)
    {
        loge("video render component wait for start finish timeout.");
        return -1;
    }
    
    return p->nStartReply;
}


int VideoRenderCompStop(VideoRenderComp* v)
{
    VideoRenderCompContext* p;
    Message                 msg;
    
    p = (VideoRenderCompContext*)v;
    
    logv("video render component stopping");
    
    msg.messageId = MESSAGE_ID_STOP;
    msg.params[0] = (unsigned int)&p->stopMsgReplySem;
    msg.params[1] = (unsigned int)&p->nStopReply;
    msg.params[2] = msg.params[3] = 0;
    
    if(MessageQueuePostMessage(p->mq, &msg) != 0)
    {
        loge("fatal error, video render component post message fail.");
        abort();
    }
    
    if(SemTimedWait(&p->stopMsgReplySem, -1) < 0)
    {
        loge("video render component wait for stop finish timeout.");
        return -1;
    }
    
    return p->nStopReply;
}


int VideoRenderCompPause(VideoRenderComp* v)
{
    VideoRenderCompContext* p;
    Message                 msg;
    
    p = (VideoRenderCompContext*)v;
    
    logv("video render component pausing");
    
    msg.messageId = MESSAGE_ID_PAUSE;
    msg.params[0] = (unsigned int)&p->pauseMsgReplySem;
    msg.params[1] = (unsigned int)&p->nPauseReply;
    msg.params[2] = msg.params[3] = 0;
    
    if(MessageQueuePostMessage(p->mq, &msg) != 0)
    {
        loge("fatal error, video render component post message fail.");
        abort();
    }
    
    if(SemTimedWait(&p->pauseMsgReplySem, -1) < 0)
    {
        loge("video render component wait for pause finish timeout.");
        return -1;
    }
    
    return p->nPauseReply;
}


enum EPLAYERSTATUS VideoRenderCompGetStatus(VideoRenderComp* v)
{
    VideoRenderCompContext* p;
    p = (VideoRenderCompContext*)v;
    return p->eStatus;
}


int VideoRenderCompReset(VideoRenderComp* v)
{
    VideoRenderCompContext* p;
    Message                 msg;
    
    p = (VideoRenderCompContext*)v;
    
    logv("video render component reseting");
    
    msg.messageId = MESSAGE_ID_RESET;
    msg.params[0] = (unsigned int)&p->resetMsgReplySem;
    msg.params[1] = (unsigned int)&p->nResetReply;
    msg.params[2] = msg.params[3] = 0;
    
    if(MessageQueuePostMessage(p->mq, &msg) != 0)
    {
        loge("fatal error, video render component post message fail.");
        abort();
    }
    
    if(SemTimedWait(&p->resetMsgReplySem, -1) < 0)
    {
        loge("video render component wait for reset finish timeout.");
        return -1;
    }
    
    return p->nResetReply;
}


int VideoRenderCompSetEOS(VideoRenderComp* v)
{
    VideoRenderCompContext* p;
    Message                 msg;
    
    p = (VideoRenderCompContext*)v;
    
    logv("video render component setting EOS.");
    
    msg.messageId = MESSAGE_ID_EOS;
    msg.params[0] = (unsigned int)&p->eosMsgReplySem;
    msg.params[1] = msg.params[2] = msg.params[3] = 0;
    
    if(MessageQueuePostMessage(p->mq, &msg) != 0)
    {
        loge("fatal error, video render component post message fail.");
        abort();
    }
    
    if(SemTimedWait(&p->eosMsgReplySem, -1) < 0)
    {
        loge("video render component wait for setting eos finish timeout.");
        return -1;
    }
    
    return 0;
}


int VideoRenderCompSetCallback(VideoRenderComp* v, PlayerCallback callback, void* pUserData)
{
    VideoRenderCompContext* p;
    
    p = (VideoRenderCompContext*)v;
    
    p->callback  = callback;
    p->pUserData = pUserData;
    
    return 0;
}


int VideoRenderCompSetTimer(VideoRenderComp* v, AvTimer* timer)
{
    VideoRenderCompContext* p;
    p = (VideoRenderCompContext*)v;
    p->pAvTimer  = timer;
    return 0;
}


int VideoRenderCompSetWindow(VideoRenderComp* v, void* pNativeWindow)
{
    VideoRenderCompContext* p;
    Message                 msg;
    
    p = (VideoRenderCompContext*)v;
    
    logv("video render component setting window.");
    
    msg.messageId = MESSAGE_ID_SET_WINDOW;
    msg.params[0] = (unsigned int)&p->setWindowReplySem;
    msg.params[1] = 0;
    msg.params[2] = (unsigned int)pNativeWindow;
    msg.params[3] = 0;
    
    if(MessageQueuePostMessage(p->mq, &msg) != 0)
    {
        loge("fatal error, video render component post message fail.");
        abort();
    }
    
    if(SemTimedWait(&p->setWindowReplySem, -1) < 0)
    {
        loge("video render component wait for setting window finish timeout.");
        return -1;
    }
    
    return 0;
}


int VideoRenderCompSetDecodeComp(VideoRenderComp* v, VideoDecComp* d)
{
    VideoRenderCompContext* p;
    p = (VideoRenderCompContext*)v;
    p->pDecComp  = d;
    
    VideoDecCompSetVideoRenderCallback(p->pDecComp,VideoRenderCompCallback,(void*)p);
    return 0;
}


int VideoRenderSet3DMode(VideoRenderComp* v, 
                         enum EPICTURE3DMODE ePicture3DMode,
                         enum EDISPLAY3DMODE eDisplay3DMode)
{
    VideoRenderCompContext* p;
    Message                 msg;
    
    p = (VideoRenderCompContext*)v;
    
    logv("video render component setting 3d mode.");
    
    msg.messageId = MESSAGE_ID_SET_3D_MODE;
    msg.params[0] = (unsigned int)&p->set3DModeReplySem;
    msg.params[1] = (unsigned int)&p->nSet3DModeReply;
    msg.params[2] = (unsigned int)ePicture3DMode;
    msg.params[3] = (unsigned int)eDisplay3DMode;
    
    if(MessageQueuePostMessage(p->mq, &msg) != 0)
    {
        loge("fatal error, video render component post message fail.");
        abort();
    }
    
    if(SemTimedWait(&p->set3DModeReplySem, -1) < 0)
    {
        loge("video render component wait for setting 3d mode finish timeout.");
        return -1;
    }
    
    return p->nSet3DModeReply;
}


int VideoRenderGet3DMode(VideoRenderComp* v, 
                         enum EPICTURE3DMODE* ePicture3DMode,
                         enum EDISPLAY3DMODE* eDisplay3DMode)
{
    VideoRenderCompContext* p;
    p = (VideoRenderCompContext*)v;
    *ePicture3DMode = p->ePicture3DMode;
    *eDisplay3DMode = p->eDisplay3DMode;
    return 0;
}


int VideoRenderVideoHide(VideoRenderComp* v, int bHideVideo)
{
    VideoRenderCompContext* p;
    Message                 msg;
    
    p = (VideoRenderCompContext*)v;
    
    logv("video render component setting video hide(%d).", bHideVideo);
    
    msg.messageId = MESSAGE_ID_SET_VIDEO_HIDE;
    msg.params[0] = (unsigned int)&p->setHideVideoSem;
    msg.params[1] = 0;
    msg.params[2] = bHideVideo;
    msg.params[3] = 0;
    
    if(MessageQueuePostMessage(p->mq, &msg) != 0)
    {
        loge("fatal error, video render component post message fail.");
        abort();
    }
    
    if(SemTimedWait(&p->setHideVideoSem, -1) < 0)
    {
        loge("video render component wait for setting 3d mode finish timeout.");
        return -1;
    }
    
    return 0;
}


int VideoRenderSetHoldLastPicture(VideoRenderComp* v, int bHold)
{
    VideoRenderCompContext* p;
    Message                 msg;
    
    p = (VideoRenderCompContext*)v;
    
    logv("video render component setting hold last picture(bHold=%d).", bHold);
    
    msg.messageId = MESSAGE_ID_SET_HOLD_LAST_PICTURE;
    msg.params[0] = (unsigned int)&p->setHoldLastPictureSem;
    msg.params[1] = 0;
    msg.params[2] = bHold;
    msg.params[3] = 0;
    
    if(MessageQueuePostMessage(p->mq, &msg) != 0)
    {
        loge("fatal error, video render component post message fail.");
        abort();
    }
    
    if(SemTimedWait(&p->setHoldLastPictureSem, -1) < 0)
    {
        loge("video render component wait for setting 3d mode finish timeout.");
        return -1;
    }
    
    return 0;
}

void VideoRenderCompSetProtecedFlag(VideoRenderComp* v, int bProtectedFlag)
{
    VideoRenderCompContext* p;
    
    p = (VideoRenderCompContext*)v;

    p->bProtectedBufferFlag = bProtectedFlag;

    return ;
}


int VideoRenderCompSetSyncFirstPictureFlag(VideoRenderComp* v, int bSyncFirstPictureFlag)
{
    //*TODO
    return 0;
}

static void* VideoRenderThread(void* arg)
{
    VideoRenderCompContext* p;
    Message                 msg;
    int                     ret;
    sem_t*                  pReplySem;
    int*                    pReplyValue;
    int64_t                 nCurTime;
    int64_t                 nWaitTime;
    int                     bFirstPictureShowed;
    int                     bFirstPtsNotified;
    VideoPicture*           pPicture;
    VideoPicture*           pPrePicture;
    VideoPicture*           pSecondPictureOf3DMode;
    VideoPicture*           pLayerBuffer;
    VideoPicture*           pSecondLayerBufferOf3DMode;
    int                     bHideVideo;
    int                     bHoldLastPicture;
    int                     bNeedResetLayerParams;
    VideoPicture*           pPreLayerBuffer;
    int                     nLayerBufferMode;
    VideoBufferInfoT*       pCurVideoBufferInfo;
    VideoBufferInfoT*       pPreVideoBufferInfo;
    int                     nDeinterlaceDispNum;
        
    p = (VideoRenderCompContext*)arg;
    bFirstPictureShowed        = 0;
    bFirstPtsNotified          = 0;
    pPicture                   = NULL;
    pPrePicture                = NULL;
    pSecondPictureOf3DMode     = NULL;
    pLayerBuffer               = NULL;
    pSecondLayerBufferOf3DMode = NULL;
    bHideVideo                 = 0;
    bHoldLastPicture           = 0;
    bNeedResetLayerParams      = 0;
    pPreLayerBuffer            = NULL;
    nLayerBufferMode           = 0;
    pCurVideoBufferInfo        = NULL;
    pPreVideoBufferInfo        = NULL;
    nDeinterlaceDispNum        = 1;
    
    while(1)
    {
        if(MessageQueueGetMessage(p->mq, &msg) < 0)
        {
            loge("get message fail.");
            continue;
        }
        
process_message:
        pReplySem   = (sem_t*)msg.params[0];
        pReplyValue = (int*)msg.params[1];
        
        if(msg.messageId == MESSAGE_ID_START)
        {
            logi("process MESSAGE_ID_START message");
            if(p->eStatus == PLAYER_STATUS_STARTED)
            {
                logw("already in started status.");
                PostRenderMessage(p->mq);
                *pReplyValue = -1;
                sem_post(pReplySem);
                continue;
            }
            
            if(p->eStatus == PLAYER_STATUS_STOPPED)
            {
                bFirstPictureShowed = 0;
                bFirstPtsNotified   = 0;
                p->bEosFlag = 0;
            }
            
            //* send a render message to start decoding.
            PostRenderMessage(p->mq);
            
            p->eStatus = PLAYER_STATUS_STARTED;
            *pReplyValue = 0;
            sem_post(pReplySem);
        }
        else if(msg.messageId == MESSAGE_ID_STOP)
        {
            logi("process MESSAGE_ID_STOP message");
            if(p->eStatus == PLAYER_STATUS_STOPPED)
            {
                logw("already in stopped status.");
                *pReplyValue = -1;
                sem_post(pReplySem);
                continue;
            }
            
            //* return buffers before stop.
            if(pPicture != NULL && pCurVideoBufferInfo != NULL)
            {
                if(p->pLayerCtrl != NULL)
                {
                    pthread_mutex_lock(&p->layerCtrlMutex);
                    LayerQueueBuffer(p->pLayerCtrl,pPicture, 0);
                    pthread_mutex_unlock(&p->layerCtrlMutex);
                }

                pCurVideoBufferInfo->bDisplayedFlag = 1;
                pPicture = NULL;
                pCurVideoBufferInfo = NULL;
            }

            if(pPrePicture != NULL && pPreVideoBufferInfo != NULL)
            {
                if(p->pLayerCtrl != NULL)
                {
                    pthread_mutex_lock(&p->layerCtrlMutex);
                    LayerQueueBuffer(p->pLayerCtrl, pPrePicture, 0);
                    pthread_mutex_unlock(&p->layerCtrlMutex);
                }

                pPreVideoBufferInfo->bDisplayedFlag = 1;
                pPrePicture = NULL;
                pPreVideoBufferInfo = NULL;
            }

            //* we should queue all the buffer to gpu
            pCurVideoBufferInfo = RequestDisplayPicture(p);
            while(pCurVideoBufferInfo != NULL)
            {
                pPicture = &pCurVideoBufferInfo->mVideoPicture;
                pthread_mutex_lock(&p->layerCtrlMutex);
                LayerQueueBuffer(p->pLayerCtrl,pPicture, 0);
                pthread_mutex_unlock(&p->layerCtrlMutex);
                pCurVideoBufferInfo->bDisplayedFlag = 1;

                pCurVideoBufferInfo = RequestDisplayPicture(p);
            }

            pPicture               = NULL;
            pCurVideoBufferInfo    = NULL;
            p->bIsSecondStreamFlag      = 0;
            p->nBufferOwnedByDecoderNum = 0;
            
            ReturnBufferToGpu(p);
            
            if(p->pLayerCtrl != NULL)
            {
                if(bHoldLastPicture)
                    LayerCtrlHoldLastPicture(p->pLayerCtrl, 1);
                else
                {
                    LayerCtrlHoldLastPicture(p->pLayerCtrl, 0);
                    if(LayerCtrlIsVideoShow(p->pLayerCtrl) == 1)
                        LayerCtrlHideVideo(p->pLayerCtrl);
                }
            }
               
            //* set status to stopped.
            p->eStatus = PLAYER_STATUS_STOPPED;
            *pReplyValue = 0;
            sem_post(pReplySem);
        }
        else if(msg.messageId == MESSAGE_ID_PAUSE)
        {
            logi("process MESSAGE_ID_PAUSE message");
            if(p->eStatus != PLAYER_STATUS_STARTED  &&
               !(p->eStatus == PLAYER_STATUS_PAUSED && bFirstPictureShowed == 0))
            {
                logw("not in started status, pause operation invalid.");
                *pReplyValue = -1;
                sem_post(pReplySem);
                continue;
            }
            
            //* set status to paused.
            p->eStatus = PLAYER_STATUS_PAUSED;
            if(bFirstPictureShowed == 0)
                PostRenderMessage(p->mq);   //* post a decode message to decode the first picture.
            *pReplyValue = 0;
            sem_post(pReplySem);
        }
        else if(msg.messageId == MESSAGE_ID_QUIT)
        {
            logi("process MESSAGE_ID_QUIT message");
            //* return buffers before quit.
            if(pPicture != NULL && pCurVideoBufferInfo != NULL)
            {
                if(p->pLayerCtrl != NULL)
                {
                    pthread_mutex_lock(&p->layerCtrlMutex);
                    LayerQueueBuffer(p->pLayerCtrl, pPicture, 0);
                    pthread_mutex_unlock(&p->layerCtrlMutex);
                }

                pCurVideoBufferInfo->bDisplayedFlag = 1;
                pPicture = NULL;
                pCurVideoBufferInfo = NULL;
            }

            if(pPrePicture != NULL && pPreVideoBufferInfo != NULL)
            {
                if(p->pLayerCtrl != NULL)
                {
                    pthread_mutex_lock(&p->layerCtrlMutex);
                    LayerQueueBuffer(p->pLayerCtrl, pPrePicture, 0);
                    pthread_mutex_unlock(&p->layerCtrlMutex);
                }

                pPreVideoBufferInfo->bDisplayedFlag = 1;
                pPrePicture = NULL;
                pPreVideoBufferInfo = NULL;
            }

            //* we should queue all the buffer to gpu
            pCurVideoBufferInfo = RequestDisplayPicture(p);
            while(pCurVideoBufferInfo != NULL)
            {
                pPicture = &pCurVideoBufferInfo->mVideoPicture;
                pthread_mutex_lock(&p->layerCtrlMutex);
                LayerQueueBuffer(p->pLayerCtrl, pPicture, 0);
                pthread_mutex_unlock(&p->layerCtrlMutex);
                pCurVideoBufferInfo->bDisplayedFlag = 1;

                pCurVideoBufferInfo = RequestDisplayPicture(p);
            }

            pPicture               = NULL;
            pCurVideoBufferInfo    = NULL;
            p->bIsSecondStreamFlag      = 0;
            p->nBufferOwnedByDecoderNum = 0;
            
            ReturnBufferToGpu(p);
            
            sem_post(pReplySem);
            p->eStatus = PLAYER_STATUS_STOPPED;
            break;
        }
        else if(msg.messageId == MESSAGE_ID_RESET)
        {
            logi("process MESSAGE_ID_RESET message");
            //* return buffers before quit.
            if(pPicture != NULL && pCurVideoBufferInfo != NULL)
            {
                if(p->pLayerCtrl != NULL)
                {
                    pthread_mutex_lock(&p->layerCtrlMutex);
                    LayerQueueBuffer(p->pLayerCtrl, pPicture, 0);
                    pthread_mutex_unlock(&p->layerCtrlMutex);
                }

                pCurVideoBufferInfo->bDisplayedFlag = 1;
                pPicture = NULL;
                pCurVideoBufferInfo = NULL;
            }

            if(pPrePicture != NULL && pPreVideoBufferInfo != NULL)
            {
                if(p->pLayerCtrl != NULL)
                {
                    pthread_mutex_lock(&p->layerCtrlMutex);
                    LayerQueueBuffer(p->pLayerCtrl, pPrePicture, 0);
                    pthread_mutex_unlock(&p->layerCtrlMutex);
                }

                pPreVideoBufferInfo->bDisplayedFlag = 1;
                pPrePicture = NULL;
                pPreVideoBufferInfo = NULL;
            }

            //* we should queue all the buffer to gpu
            pCurVideoBufferInfo = RequestDisplayPicture(p);
            while(pCurVideoBufferInfo != NULL)
            {
                pPicture = &pCurVideoBufferInfo->mVideoPicture;
                pthread_mutex_lock(&p->layerCtrlMutex);
                LayerQueueBuffer(p->pLayerCtrl, pPicture, 0);
                pthread_mutex_unlock(&p->layerCtrlMutex);
                pCurVideoBufferInfo->bDisplayedFlag = 1;

                pCurVideoBufferInfo = RequestDisplayPicture(p);
            }

            pPicture = NULL;
            pCurVideoBufferInfo = NULL;
            
            ReturnBufferToGpu(p);
            
            //* clear the eos flag.
            p->bEosFlag                 = 0;
            p->bIsSecondStreamFlag      = 0;
            p->nBufferOwnedByDecoderNum = 0;
            bFirstPictureShowed = 0;
            bFirstPtsNotified   = 0;
            *pReplyValue        = 0;
            sem_post(pReplySem);
            
            //* send a message to continue the thread.
            if(p->eStatus == PLAYER_STATUS_STARTED ||
               (p->eStatus == PLAYER_STATUS_PAUSED && bFirstPictureShowed == 0))
                PostRenderMessage(p->mq);
        }
        else if(msg.messageId == MESSAGE_ID_EOS)
        {
            logi("process MESSAGE_ID_EOS message");
            p->bEosFlag = 1;
            sem_post(pReplySem);
            
            //* send a message to continue the thread.
            if(p->eStatus == PLAYER_STATUS_STARTED ||
               (p->eStatus == PLAYER_STATUS_PAUSED && bFirstPictureShowed == 0))
                PostRenderMessage(p->mq);
        }
        else if(msg.messageId == MESSAGE_ID_SET_WINDOW)
        {
            logi("process MESSAGE_ID_SET_WINDOW message");
            //* return buffer to old layer.
            if(pPicture != NULL && pCurVideoBufferInfo != NULL)
            {
                if(p->pLayerCtrl != NULL)
                    LayerQueueBuffer(p->pLayerCtrl, pPicture, 0);

                pCurVideoBufferInfo->bDisplayedFlag = 1;
                pPicture = NULL;
                pCurVideoBufferInfo = NULL;
            }
            
            p->pNativeWindow = (void*)msg.params[2];

            if(p->pLayerCtrl != NULL)
            {
                //* On the new-displayer of android, we not call LayerRelease,
                //* just reset nativeWindow.
#if(CONFIG_OS == OPTION_OS_ANDROID)                
                //* we should queue all the buffer to gpu
                pCurVideoBufferInfo = RequestDisplayPicture(p);
                while(pCurVideoBufferInfo != NULL)
                {
                    pPicture = &pCurVideoBufferInfo->mVideoPicture;
                    pthread_mutex_lock(&p->layerCtrlMutex);
                    LayerQueueBuffer(p->pLayerCtrl, pPicture, 0);
                    pthread_mutex_unlock(&p->layerCtrlMutex);
                    pCurVideoBufferInfo->bDisplayedFlag = 1;

                    pCurVideoBufferInfo = RequestDisplayPicture(p);
                }

                pPicture = NULL;
                pCurVideoBufferInfo = NULL;
                p->bIsSecondStreamFlag      = 0;
                p->nBufferOwnedByDecoderNum = 0;
                
                ReturnBufferToGpu(p);
                
                pthread_mutex_lock(&p->layerCtrlMutex);
                LayerResetNativeWindow(p->pLayerCtrl,p->pNativeWindow);
                pthread_mutex_unlock(&p->layerCtrlMutex);
                
                goto set_nativeWindow_exit;
#else
                LayerRelease(p->pLayerCtrl, 0);
                p->pLayerCtrl = NULL;
#endif                
            }
            
            
            //* on linux, pNativeWindow == NULL, and the LayerCtrl module will 
            //* create a layer to show video picture.
#if CONFIG_OS == OPTION_OS_ANDROID
            if(p->pNativeWindow != NULL)
#endif
            {
                p->pLayerCtrl = LayerInit(p->pNativeWindow,p->bProtectedBufferFlag);
            }
            bNeedResetLayerParams = 1;

            //* we should set layer info here if it hadn't set it
            if(p->bHadSetLayerInfoFlag == 0 && p->mFbmBufInfo.nBufNum != 0)
            {
                LayerSetIsSoftDecoderFlag(p->pLayerCtrl, p->mFbmBufInfo.bIsSoftDecoderFlag);
                LayerSetVideoWithTwoStreamFlag(p->pLayerCtrl,p->bVideoWithTwoStream);
                LayerSetExpectPixelFormat(p->pLayerCtrl,(enum EPIXELFORMAT)p->mFbmBufInfo.ePixelFormat);
                LayerSetPictureSize(p->pLayerCtrl, p->mFbmBufInfo.nBufWidth, p->mFbmBufInfo.nBufHeight);
                LayerSetBufferCount(p->pLayerCtrl, p->mFbmBufInfo.nBufNum);
                p->bHadSetLayerInfoFlag    = 1;                
                p->nVideoBufferWidth       = p->mFbmBufInfo.nBufWidth;
                p->nVideoBufferHeight      = p->mFbmBufInfo.nBufHeight;
				p->mFbmBufInfo.nAlignValue = p->nGpuAlign;
            }

set_nativeWindow_exit:
    
            sem_post(pReplySem);
            
            //* send a message to continue the thread.
            if(p->eStatus == PLAYER_STATUS_STARTED ||
               (p->eStatus == PLAYER_STATUS_PAUSED && bFirstPictureShowed == 0))
                PostRenderMessage(p->mq);
        }
        else if(msg.messageId == MESSAGE_ID_SET_3D_MODE)
        {
            logi("process MESSAGE_ID_SET_3D_MODE message");
            p->ePicture3DMode = (enum EPICTURE3DMODE)msg.params[2];
            p->eDisplay3DMode = (enum EDISPLAY3DMODE)msg.params[3];
            
            //* now , we no need to set 3D mode to nativeWindow , the app will set it
            #if 0
            if(p->pLayerCtrl != NULL)
            {
                LayerSetPicture3DMode(p->pLayerCtrl, (enum EPICTURE3DMODE)msg.params[2]);
                *pReplyValue = LayerSetDisplay3DMode(p->pLayerCtrl, (enum EDISPLAY3DMODE)msg.params[3]);
            }
            else
            {
                logw("window not set yet, can not set 3d mode.");
                *pReplyValue = -1;
            }
            #else
            *pReplyValue = 0;
            #endif
            
            sem_post(pReplySem);
            
            //* send a message to continue the thread.
            if(p->eStatus == PLAYER_STATUS_STARTED ||
               (p->eStatus == PLAYER_STATUS_PAUSED && bFirstPictureShowed == 0))
                PostRenderMessage(p->mq);
        }
        else if(msg.messageId == MESSAGE_ID_SET_VIDEO_HIDE)
        {
            logi("process MESSAGE_ID_SET_VIDEO_HIDE message");
            bHideVideo = msg.params[2];
            if(bHideVideo == 1) //* hide video.
            {
                if(p->pLayerCtrl != NULL && LayerCtrlIsVideoShow(p->pLayerCtrl) == 1)
                    LayerCtrlHideVideo(p->pLayerCtrl);
            }
            else
            {
                if(p->pLayerCtrl != NULL && 
                   LayerCtrlIsVideoShow(p->pLayerCtrl) == 0 && 
                   bFirstPictureShowed == 1)
                    LayerCtrlShowVideo(p->pLayerCtrl);
            }
            sem_post(pReplySem);
            
            //* send a message to continue the thread.
            if(p->eStatus == PLAYER_STATUS_STARTED ||
               (p->eStatus == PLAYER_STATUS_PAUSED && bFirstPictureShowed == 0))
                PostRenderMessage(p->mq);
        }
        else if(msg.messageId == MESSAGE_ID_SET_HOLD_LAST_PICTURE)
        {
            logi("process MESSAGE_ID_SET_HOLD_LAST_PICTURE message");
            bHoldLastPicture = msg.params[2];
            sem_post(pReplySem);
            
            //* send a message to continue the thread.
            if(p->eStatus == PLAYER_STATUS_STARTED ||
               (p->eStatus == PLAYER_STATUS_PAUSED && bFirstPictureShowed == 0))
                PostRenderMessage(p->mq);
        }
        else if(msg.messageId == MESSAGE_ID_SET_LAYER_INFO)
        {
            logd("process MESSAGE_ID_SET_LAYER_INFO message, p->pLayerCtrl = %p, bHadSetLayerInfoFlag = %d",
                 p->pLayerCtrl,
                 p->bHadSetLayerInfoFlag);

            FbmBufInfo* pFbmBufInfo     = (FbmBufInfo*)msg.params[2];
            
            //* We check whether it is a 3D stream here,
            //* because Layer must know whether it 3D stream at the beginning;
            p->bVideoWithTwoStream = IsVideoWithTwoStream(p->pDecComp);

            if(p->bVideoWithTwoStream == -1)
                 p->bVideoWithTwoStream = 0;

            if(p->pLayerCtrl != NULL && p->bHadSetLayerInfoFlag == 0)
            {
                logd("video size info: nWidth[%d], nHeight[%d],nBufferCount[%d], offset : %d, %d, %d, %d",
                      pFbmBufInfo->nBufWidth,
                      pFbmBufInfo->nBufHeight,
                      pFbmBufInfo->nBufNum,
                      pFbmBufInfo->nBufLeftOffset,
                      pFbmBufInfo->nBufRightOffset,
                      pFbmBufInfo->nBufTopOffset,
                      pFbmBufInfo->nBufBottomOffset);

                //* we init deinterlace device here
                if(p->di != NULL && pFbmBufInfo->bProgressiveFlag == 0 && USE_DETNTERLACE)
                {
                    if (p->di->init() == 0)
                    {
                        int di_flag = p->di->flag();
                        p->bDeinterlaceFlag   = 1;
                        nDeinterlaceDispNum   = (di_flag == DE_INTERLACE_HW) ? 2 : 1;
                        LayerSetExpectPixelFormat(p->pLayerCtrl, p->di->expectPixelFormat());
                    }
                    else
                    {
                        logw(" open deinterlace failed , we not to use deinterlace!");
                    }
                }
				
				LayerSetIsSoftDecoderFlag(p->pLayerCtrl, pFbmBufInfo->bIsSoftDecoderFlag);
                LayerSetVideoWithTwoStreamFlag(p->pLayerCtrl,p->bVideoWithTwoStream);
                LayerSetPictureSize(p->pLayerCtrl, pFbmBufInfo->nBufWidth, pFbmBufInfo->nBufHeight);

                //* we should add 2 buffer if it is interlace video
                if(p->bDeinterlaceFlag == 1)
                {
                    LayerSetBufferCount(p->pLayerCtrl, pFbmBufInfo->nBufNum + 2);
                }
                else
                {
                    LayerSetExpectPixelFormat(p->pLayerCtrl,(enum EPIXELFORMAT)pFbmBufInfo->ePixelFormat);
                    LayerSetBufferCount(p->pLayerCtrl, pFbmBufInfo->nBufNum);
                }
                p->bHadSetLayerInfoFlag  = 1;
                pFbmBufInfo->nAlignValue = p->nGpuAlign;
                p->nVideoBufferWidth     = pFbmBufInfo->nBufWidth;
                p->nVideoBufferHeight    = pFbmBufInfo->nBufHeight;
                p->nVideoBufferCount     = pFbmBufInfo->nBufNum;
            }
            
            //* send a render message to start decoding.
            PostRenderMessage(p->mq);
        }
        else if(msg.messageId == MESSAGE_ID_RENDER)
        {
            logi("process MESSAGE_ID_RENDER message");
            
            if(p->eStatus != PLAYER_STATUS_STARTED && 
              !(p->eStatus == PLAYER_STATUS_PAUSED && bFirstPictureShowed == 0))
            {
                logw("not in started status, render message ignored.");
                continue;
            }
            
            //* we should not render picture when have not set width,
            //* height,buffercount to layer
            if(p->bHadSetLayerInfoFlag != 1)
                continue;

            //* check whether have buffer need to return to gpu
            ReturnBufferToGpu(p);
            
            if(pPicture == NULL)
            {
                //*******************************
                //* 1. get picture from display queue.
                //*******************************
                while(pPicture == NULL)
                {
                    pCurVideoBufferInfo = RequestDisplayPicture(p);
                    if(pCurVideoBufferInfo != NULL)
                        pPicture = &pCurVideoBufferInfo->mVideoPicture;

                    logv("get picture = %p",pPicture);
                    if(pPicture != NULL || p->bEosFlag)
                    {
                        break;
                    }

                    if(p->bResolutionChange)
                    {
                        //* reopen the layer.
                        if(p->pLayerCtrl != NULL)
                        {
                            ReturnBufferToGpu(p);
                            
                            if(bFirstPictureShowed == 1)
                                LayerCtrlHoldLastPicture(p->pLayerCtrl, 1);
                            LayerRelease(p->pLayerCtrl, 0);
                            p->pLayerCtrl = NULL;
                            
                            p->pLayerCtrl = LayerInit(p->pNativeWindow,p->bProtectedBufferFlag);
                            if(p->pLayerCtrl != NULL)
                                bNeedResetLayerParams = 1;

                            p->pDisplayerBufferListHead = NULL;
                            memset(&p->mVideoBufferInfo, 0, sizeof(VideoBufferInfoT)*MAX_VIDEO_BUFFER_NUM);
                            memset(&p->displayBufferNode, 0, sizeof(VDisplayBufferNodeT)*MAX_VIDEO_BUFFER_NUM);
                        }

                        p->bResolutionChange    = 0;
                        p->bHadSetLayerInfoFlag = 0;
                        
                        //* reopen the video engine.
                        VideoDecCompReopenVideoEngine(p->pDecComp);
                    }

                    ret = MessageQueueTryGetMessage(p->mq, &msg, 5); //* wait for 5ms if no message come.
                    if(ret == 0)    //* new message come, quit loop to process.
                        goto process_message;
                }
                
                //*****************************************************************
                //* 2. handle EOS, pPicture should not be NULL except bEosFlag==1.
                //*****************************************************************
                if(pPicture == NULL)
                {
                    if(p->bEosFlag == 1)
                    {
                        p->callback(p->pUserData, PLAYER_VIDEO_RENDER_NOTIFY_EOS, NULL);
                        continue;
                    }
                    else
                    {
                        loge("pPicture=NULL but bEosFlag is not set, shouldn't run here.");
                        abort();
                    }
                }
                
                //********************************************
                //* 3. initialize layer and notify video size.
                //********************************************
                if(bFirstPictureShowed == 0 || bNeedResetLayerParams == 1)
                {
                    int size[4];

                    if((pPicture->nRightOffset - pPicture->nLeftOffset) > 0 && 
                       (pPicture->nBottomOffset - pPicture->nTopOffset) > 0)
		        	{
                        size[0] = pPicture->nRightOffset - pPicture->nLeftOffset;
                        size[1] = pPicture->nBottomOffset - pPicture->nTopOffset;
                        size[2] = 0;
                        size[3] = 0;
                        p->callback(p->pUserData, PLAYER_VIDEO_RENDER_NOTIFY_VIDEO_SIZE, (void*)size);
                    
	        		    size[0] = pPicture->nLeftOffset;
					    size[1] = pPicture->nTopOffset;
		        		size[2] = pPicture->nRightOffset - pPicture->nLeftOffset;
						size[3] = pPicture->nBottomOffset - pPicture->nTopOffset;
                        p->callback(p->pUserData, PLAYER_VIDEO_RENDER_NOTIFY_VIDEO_CROP, (void*)size);

                        if(p->pLayerCtrl != NULL)
                        {
                            LayerSetDisplayRegion(p->pLayerCtrl,
                            		              pPicture->nLeftOffset,
                            		              pPicture->nTopOffset,
                            		              pPicture->nRightOffset - pPicture->nLeftOffset,
                            		              pPicture->nBottomOffset - pPicture->nTopOffset);
                        }
		        	}
                    else
                    {
                        logw("the offset of picture is not right, we set bufferWidht and \
                              bufferHeight as video size, this is maybe wrong, offset: %d, %d, %d, %d",
                              pPicture->nLeftOffset,pPicture->nRightOffset,
                              pPicture->nTopOffset,pPicture->nBottomOffset);
                        size[0] = pPicture->nWidth;
                        size[1] = pPicture->nHeight;
                        size[2] = 0;
                        size[3] = 0;
                        p->callback(p->pUserData, PLAYER_VIDEO_RENDER_NOTIFY_VIDEO_SIZE, (void*)size);   

                        if(p->pLayerCtrl != NULL)
                        {
                            LayerSetDisplayRegion(p->pLayerCtrl,
                            		              0,
                            		              0,
                            		              pPicture->nWidth,
                            		              pPicture->nHeight);
                        }
                    }

                    bNeedResetLayerParams = 0;
                }
                
                //************************************************************************************
                //* 4. notify the first sync frame to set timer. the first sync frame is the second
                //*    picture, the first picture need to be showed as soon as we can.(unsynchroized)
                //************************************************************************************
step_4:         
                if(bFirstPictureShowed != 0)
                {
                    if(bFirstPtsNotified == 0)
                    {
	                    //* this callback may block because the player need wait audio first frame to sync.
	                    ret = p->callback(p->pUserData, PLAYER_VIDEO_RENDER_NOTIFY_FIRST_PICTURE, (void*)&pPicture->nPts);
	                    if(ret == TIMER_DROP_VIDEO_DATA)
	                    {
	                        //* video first frame pts small (too much) than the audio, 
	                        //* discard this frame to catch up the audio.
	                        pthread_mutex_lock(&p->layerCtrlMutex);
	                        LayerQueueBuffer(p->pLayerCtrl, pPicture, 0);
                            pthread_mutex_unlock(&p->layerCtrlMutex);

                            pCurVideoBufferInfo->bDisplayedFlag = 1;
                            pPicture = NULL;
                            pCurVideoBufferInfo = NULL;

                            PostRenderMessage(p->mq);
                            continue;
	                    }
                        else if(ret == TIMER_NEED_NOTIFY_AGAIN)
                        {
                            //* waiting process for first frame sync with audio is broken by a new message to player, so the player tell us to notify again later.
                            //* post a render message to continue the rendering job after message processed.
                            ret = MessageQueueTryGetMessage(p->mq, &msg, 10); //* wait for 10ms if no message come.
                            if(ret == 0)    //* new message come, quit loop to process.
                                goto process_message;
                            PostRenderMessage(p->mq);
                            continue;
                        }
	                    bFirstPtsNotified = 1;
                    }
                }

                //******************************************************
                //* 5. dequeue buffer from layer and copy picture data.
                //******************************************************
step_5:
                if(p->bDeinterlaceFlag == 0)
                {
                    //******************************************************
                    //* 6. wait according to the presentation time stamp.
                    //******************************************************
                    if(bFirstPictureShowed != 0)    //* the first picture is showed unsychronized.
                    {
                        //* nWaitTime is in unit of ms.
                        nWaitTime = p->callback(p->pUserData, PLAYER_VIDEO_RENDER_NOTIFY_PICTURE_PTS, (void*)&pPicture->nPts);
                        if(nWaitTime > 0)
                        {
                            int nWaitTimeOnce;
                            while(nWaitTime > 0)
                            {
                                //* wait for 100ms if no message come.
                                nWaitTimeOnce = (nWaitTime>100 ? 100 : nWaitTime);
                                ret = MessageQueueTryGetMessage(p->mq, &msg, nWaitTimeOnce);
                                if(ret == 0)    //* new message come, quit loop to process.
                                {
                                    //* if pLayerCtrl==null, we should return picture before process message
                                    //* or the picture will never be returned.
                                    #if 0
                                    if(p->pLayerCtrl == NULL)
                                    {
                                        VideoDecCompReturnPicture(p->pDecComp, pPicture);
                                        pPicture = NULL;
                                        pLayerBuffer = NULL;
                                    }
                                    #endif
                                    goto process_message;
                                }
                                nWaitTime -= nWaitTimeOnce;
                            }
                        }
                    }
                    
                    //******************************************************
                    //* 7. queue buffer to show.
                    //******************************************************
                    if(p->pLayerCtrl != NULL)
                    {
                        pthread_mutex_lock(&p->layerCtrlMutex);
                        LayerQueueBuffer(p->pLayerCtrl, pPicture);
                        pthread_mutex_unlock(&p->layerCtrlMutex);
                        
                        pCurVideoBufferInfo->bDisplayedFlag = 1;
                        pPicture = NULL;
                        pCurVideoBufferInfo = NULL;
                    }
                    else
                    {
                        //* if pLayerCtrl==null , we should return picture to decoder immediately
                        #if 0
                        VideoDecCompReturnPicture(p->pDecComp, pPicture);
                        pPicture = NULL;
                        pLayerBuffer = NULL;
                        #endif
                    }

                    
                    if(p->pLayerCtrl != NULL && 
                       LayerCtrlIsVideoShow(p->pLayerCtrl) == 0 && 
                       bHideVideo == 0)
                    {
                        pthread_mutex_lock(&p->layerCtrlMutex);
                        LayerCtrlShowVideo(p->pLayerCtrl);
                        pthread_mutex_unlock(&p->layerCtrlMutex);
                    }
                    
                }
                else
                {
                    for(int nDeinterlaceTime = 0; nDeinterlaceTime < nDeinterlaceDispNum; nDeinterlaceTime++)
                    {  
                        //* deinterlace process
                        if(pPrePicture == NULL)
                        {
                            pPrePicture          = pPicture;
                            pPreVideoBufferInfo  = pCurVideoBufferInfo;
                        }

                        VideoPicture mDiOutPicture;

                        pthread_mutex_lock(&p->layerCtrlMutex);
                        int nDeRet = LayerDequeueBuffer(p->pLayerCtrl,&mDiOutPicture);
                        pthread_mutex_unlock(&p->layerCtrlMutex);

                        int nDeququeBufferCount = 0;
                        while(nDeRet != 0)
                        {
                            nDeququeBufferCount++;

                            if(nDeququeBufferCount >= 200)
                            {
                                loge("** dequeue buffer time out");
                                abort();
                            }
                            usleep(5*1000);
                            pthread_mutex_lock(&p->layerCtrlMutex);
                            nDeRet = LayerDequeueBuffer(p->pLayerCtrl,&mDiOutPicture);
                            pthread_mutex_unlock(&p->layerCtrlMutex);
                        }

                        int diret = p->di->process(pPrePicture,
                                           pPicture,
                                           &mDiOutPicture,
                                           nDeinterlaceTime);
                        if (diret != 0)
                        {
                            p->di->reset();

                            pthread_mutex_lock(&p->layerCtrlMutex);
                            LayerQueueBuffer(p->pLayerCtrl, pPrePicture, 0);
                            LayerQueueBuffer(p->pLayerCtrl, &mDiOutPicture, 0);
                            LayerReturnBuffer(p->pLayerCtrl,&mDiOutPicture);
                            pthread_mutex_unlock(&p->layerCtrlMutex);

                            pPreVideoBufferInfo->bDisplayedFlag = 1;
                		    pPrePicture = NULL;
                            nDeinterlaceTime = 0; 
                            continue;
                        }

                        pthread_mutex_lock(&p->layerCtrlMutex);
                        LayerQueueBuffer(p->pLayerCtrl,&mDiOutPicture);
                        LayerReturnBuffer(p->pLayerCtrl,&mDiOutPicture);
                        pthread_mutex_unlock(&p->layerCtrlMutex);

                        //******************************************************
                        //* 6. wait according to the presentation time stamp.
                        //******************************************************
                        if(bFirstPictureShowed != 0)    //* the first picture is showed unsychronized.
                        {
                            //* nWaitTime is in unit of ms.
                            nWaitTime = p->callback(p->pUserData, PLAYER_VIDEO_RENDER_NOTIFY_PICTURE_PTS, (void*)&pPicture->nPts);
                            if(nWaitTime > 0)
                            {
                                int nWaitTimeOnce;
                                while(nWaitTime > 0)
                                {
                                    //* wait for 100ms if no message come.
                                    nWaitTimeOnce = (nWaitTime>100 ? 100 : nWaitTime);
                                    ret = MessageQueueTryGetMessage(p->mq, &msg, nWaitTimeOnce);
                                    if(ret == 0)    //* new message come, quit loop to process.
                                    {
                                        if(nDeinterlaceTime == 1 && p->pLayerCtrl != NULL && pPrePicture != NULL)
                                        {
                                            pthread_mutex_lock(&p->layerCtrlMutex);
                                            LayerQueueBuffer(p->pLayerCtrl, pPicture, 0);
                                            pthread_mutex_unlock(&p->layerCtrlMutex);
                                            
                                            pCurVideoBufferInfo->bDisplayedFlag = 1;
                                            pPicture = NULL;
                                            pCurVideoBufferInfo = NULL;
                                        }
                                        
                                        goto process_message;
                                    }
                                    nWaitTime -= nWaitTimeOnce;
                                }
                            }
                            else if(nWaitTime < -5 && p->bDeinterlaceFlag == 1)
                            {
    							//* if it is deinterlace and expired, we should drop it
                                #if 1
                                pPreLayerBuffer = pLayerBuffer;
                                pLayerBuffer    = NULL;
                                if(nDeinterlaceTime == 0)
                                    continue;
                                else
                                    break;
                                #endif
                                    
                            }
                        }
                        
                        if(p->pLayerCtrl != NULL && 
                           LayerCtrlIsVideoShow(p->pLayerCtrl) == 0 && 
                           bHideVideo == 0)
                        {
                            pthread_mutex_lock(&p->layerCtrlMutex);
                            LayerCtrlShowVideo(p->pLayerCtrl);
                            pthread_mutex_unlock(&p->layerCtrlMutex);
                        }
                        
                    }

                    if(pPicture != pPrePicture && pPrePicture != NULL)
                    {
                        pthread_mutex_lock(&p->layerCtrlMutex);
                        LayerQueueBuffer(p->pLayerCtrl, pPrePicture, 0);
                        pthread_mutex_unlock(&p->layerCtrlMutex);

                        pPreVideoBufferInfo->bDisplayedFlag = 1;
                    }
                    pPrePicture         = pPicture;
                    pPreVideoBufferInfo = pCurVideoBufferInfo;
                    pPicture            = NULL;
                    pCurVideoBufferInfo = NULL;
                    
                }

                if(bFirstPictureShowed == 0)
                    bFirstPictureShowed = 1;
                
                if(p->eStatus == PLAYER_STATUS_STARTED)
                    PostRenderMessage(p->mq);
                else
                {
                    //* p->eStatus == PLAYER_STATUS_PAUSED && bFirstPictureShowed == 0
                    //* need to show the first picture as soon as we can after seek.
                    logi("first picture showed at paused status.");
                }
                continue;
            }
            else    //* pPicture or pLayerBuffer not NULL.
            {
                //* continue last process.
                if(pLayerBuffer != NULL)
                    goto step_5;    
                else if(bFirstPtsNotified == 1)
                    goto step_5;
                else
                    goto step_4;
            }
        }
        else
        {
            //* unknown message.
            if(pReplyValue != NULL)
                *pReplyValue = -1;
            if(pReplySem)
                sem_post(pReplySem);
        }
    }

    if(p->pLayerCtrl != NULL)
    {
        LayerRelease(p->pLayerCtrl, 0);
        p->pLayerCtrl = NULL;
    }
    
    ret = 0;
    pthread_exit(&ret);
    
    return NULL;
}


static void PostRenderMessage(MessageQueue* mq)
{
    if(MessageQueueGetCount(mq)<=0)
    {
        Message msg;
        msg.messageId = MESSAGE_ID_RENDER;
        msg.params[0] = msg.params[1] = msg.params[2] = msg.params[3] = 0;
        if(MessageQueuePostMessage(mq, &msg) != 0)
        {
            loge("fatal error, video render component post message fail.");
            abort();
        }
        
        return;
    }
}

static int IsVideoWithTwoStream(VideoDecComp* pDecComp)
{
    VideoStreamInfo videoStreamInfo;
    if(VideoDecCompGetVideoStreamInfo(pDecComp, &videoStreamInfo) == 0)
        return videoStreamInfo.bIs3DStream;
    else
        return -1;
}

static VideoBufferInfoT* RequestDisplayPicture(VideoRenderCompContext* p)
{
    VideoBufferInfoT*  pVideoBufferInfo = NULL;
    int i = 0;

    pthread_mutex_lock(&p->videoBufferInfoMutex);
    
    if(p->pDisplayerBufferListHead != NULL)
    {
        VDisplayBufferNodeT* pNewNode = NULL;
        pNewNode = p->pDisplayerBufferListHead;
        p->pDisplayerBufferListHead = p->pDisplayerBufferListHead->pNext;

        pVideoBufferInfo = pNewNode->pVideoBufferInfo;
        pNewNode->bUseFlag = 0;
    }
    else
    {
        pVideoBufferInfo =  NULL;
    }
    
    pthread_mutex_unlock(&p->videoBufferInfoMutex);

    return pVideoBufferInfo;
}

//* check whether have buffer which should return to Gpu
static void ReturnBufferToGpu(VideoRenderCompContext* p)
{
    int i;
    pthread_mutex_lock(&p->videoBufferInfoMutex);

    for(i = 0; i < MAX_VIDEO_BUFFER_NUM; i++)
    {
        if(p->mVideoBufferInfo[i].bUsedFlag == 1
           && p->mVideoBufferInfo[i].bDisplayedFlag == 1
           && p->mVideoBufferInfo[i].bCanReturnFlag == 1)
        {
            logv("return buffer to gpu sucess : i = %d",i);
            VideoPicture* pPicture = &p->mVideoBufferInfo[i].mVideoPicture;
            
            pthread_mutex_lock(&p->layerCtrlMutex);
            logv("call LayerReturnBuffer");
            LayerReturnBuffer(p->pLayerCtrl,pPicture);
            pthread_mutex_unlock(&p->layerCtrlMutex);

            p->mVideoBufferInfo[i].bUsedFlag      = 0;
            p->mVideoBufferInfo[i].bDisplayedFlag = 0;
            p->mVideoBufferInfo[i].bCanReturnFlag = 0;
            p->mVideoBufferInfo[i].nReturnCount   = 0;
            p->nBufferOwnedByDecoderNum--;
        }
    }

    pthread_mutex_unlock(&p->videoBufferInfoMutex);
}

static int VideoRenderCompCallback(void* pUserData, int eMessageId, void* param)
{
    VideoRenderCompContext* p;
    int i = 0;
    int j = 0;
    
    p = (VideoRenderCompContext*)pUserData;

    switch(eMessageId)
    {
        case VIDEO_RENDER_VIDEO_INFO:
        {
            logv("callback --- set video info");
            FbmBufInfo* pFbmBufInfo = (FbmBufInfo*)param;
            
            memcpy(&p->mFbmBufInfo,pFbmBufInfo,sizeof(FbmBufInfo));
            Message                 msg;
            msg.messageId = MESSAGE_ID_SET_LAYER_INFO;
            msg.params[0] = 0;
            msg.params[1] = 0;
            msg.params[2] = (unsigned int)(&p->mFbmBufInfo);
            
            if(MessageQueuePostMessage(p->mq, &msg) != 0)
            {
                loge("fatal error, video render component post message fail.");
                abort();
            }
            return 0;
        }
        case VIDEO_RENDER_REQUEST_BUFFER:
        {
            VideoPicture* pPicBufInfo;
            pPicBufInfo = (VideoPicture*)param;
            if(p->pLayerCtrl != NULL && p->bHadSetLayerInfoFlag == 1 
               && p->bVideoWithTwoStream != -1)
            {
                int ret = -1;

                //* we should limit buffers owned by decoder if it is interlace video
                if(p->bDeinterlaceFlag == 1 
                   && p->nBufferOwnedByDecoderNum >= p->nVideoBufferCount)
                {
                    return ret;
                }

                if(p->bVideoWithTwoStream == 0)
                {
                    pthread_mutex_lock(&p->layerCtrlMutex);
                    ret = LayerDequeueBuffer(p->pLayerCtrl, pPicBufInfo);
                    pthread_mutex_unlock(&p->layerCtrlMutex);
                }
                else
                {
                    if(p->bIsSecondStreamFlag == 0)
                    {
                        pthread_mutex_lock(&p->layerCtrlMutex);
                        ret = LayerDequeueBuffer(p->pLayerCtrl, pPicBufInfo);
                        pthread_mutex_unlock(&p->layerCtrlMutex);
                        if(ret == 0)
                        {
                            //* We must compute the phy address again,
                            //* because the two stream buffer mode is  Y1/Y2/UV1/UV2,
                            //* the y and uv are not together
                            pPicBufInfo->phyCBufAddr = pPicBufInfo->phyYBufAddr + p->nVideoBufferWidth*p->nVideoBufferHeight*2;
                            pPicBufInfo->pData1      = pPicBufInfo->pData0 + p->nVideoBufferWidth*p->nVideoBufferHeight*2;

                            memcpy(&p->mSecondStreamPicBufInfo,pPicBufInfo,sizeof(VideoPicture));
                            p->bIsSecondStreamFlag = 1;

                            logv("*** major stream , id = %d, phy1 = %x, phy2 = %x, pData0 = %p, pData1 = %p",
                                 pPicBufInfo->nBufId,
                                 pPicBufInfo->phyYBufAddr,
                                 pPicBufInfo->phyCBufAddr,
                                 pPicBufInfo->pData0,
                                 pPicBufInfo->pData1);
                        }
                        else
                        {
                            pPicBufInfo->phyYBufAddr   = -1;
                            pPicBufInfo->phyCBufAddr   = -1;
                            pPicBufInfo->nBufId        = -1;
                        }
                    }
                    else
                    {
                        //* compute the second video stream address
                        memcpy(pPicBufInfo,&p->mSecondStreamPicBufInfo,sizeof(VideoPicture));
                        pPicBufInfo->phyYBufAddr = pPicBufInfo->phyYBufAddr + p->nVideoBufferWidth*p->nVideoBufferHeight;
                        pPicBufInfo->phyCBufAddr = pPicBufInfo->phyCBufAddr + p->nVideoBufferWidth*p->nVideoBufferHeight/2;
                        pPicBufInfo->pData0      = pPicBufInfo->pData0 + p->nVideoBufferWidth*p->nVideoBufferHeight;
                        pPicBufInfo->pData1      = pPicBufInfo->pData1 + p->nVideoBufferWidth*p->nVideoBufferHeight/2;
                        p->bIsSecondStreamFlag   = 0;
                        ret = 0;
                        
                        logv("*** second stream , id = %d, phy1 = %x, phy2 = %x, pData0 = %p, pData1 = %p, w h = %d, %d",
                                 pPicBufInfo->nBufId,
                                 pPicBufInfo->phyYBufAddr,
                                 pPicBufInfo->phyCBufAddr,
                                 pPicBufInfo->pData0,
                                 pPicBufInfo->pData1,
                                 pPicBufInfo->nWidth,
                                 pPicBufInfo->nHeight);
                    }
                }
                
                if(ret == 0)
                {
                    p->nBufferOwnedByDecoderNum++;
                }
                
                return ret;
            }
            else
            {
                logd("the layerCtrl is null when request buffer");
                pPicBufInfo->phyYBufAddr   = -1;
                pPicBufInfo->phyCBufAddr   = -1;
                pPicBufInfo->nBufId        = -1;
                return -1;
            }
        }
        case VIDEO_RENDER_DISPLAYER_BUFFER:
        {
            pthread_mutex_lock(&p->videoBufferInfoMutex);
            
            VideoPicture* pVideoPicture = (VideoPicture*)param;
            logv("callback --- displayer buffer: phy = %x, id = %d, pts = %lld, offset: %d, %d, %d, %d,pData = %p",
                  pVideoPicture->phyYBufAddr,
                  pVideoPicture->nBufId,
                  pVideoPicture->nPts,
                  pVideoPicture->nLeftOffset,
                  pVideoPicture->nTopOffset,
                  pVideoPicture->nRightOffset,
                  pVideoPicture->nBottomOffset,
                   pVideoPicture->pData0);
            if(p->bVideoWithTwoStream == 0)
            {
                for(i = 0; i < MAX_VIDEO_BUFFER_NUM; i++)
                {
                    if(p->mVideoBufferInfo[i].bUsedFlag == 0)
                    {
                        logv("displayer buffer sucess : i = %d",i);
                        
                        memcpy(&p->mVideoBufferInfo[i].mVideoPicture,pVideoPicture,sizeof(VideoPicture));
                        p->mVideoBufferInfo[i].bUsedFlag      = 1;
                        p->mVideoBufferInfo[i].bDisplayedFlag = 0;
                        p->mVideoBufferInfo[i].bCanReturnFlag = 0;
                        
                        logv("displayer buffer sucess : i = %d, phy = %x, id = %d, pts = %lld",
                              i,
                              p->mVideoBufferInfo[i].mVideoPicture.phyYBufAddr,
                              p->mVideoBufferInfo[i].mVideoPicture.nBufId,
                              p->mVideoBufferInfo[i].mVideoPicture.nPts);
                        
                        for(j = 0; j < MAX_VIDEO_BUFFER_NUM; j++)
                        {
                            if(p->displayBufferNode[j].bUseFlag == 0)
                            {
                                p->displayBufferNode[j].bUseFlag         = 1;
                                p->displayBufferNode[j].pVideoBufferInfo = &p->mVideoBufferInfo[i];
                                p->displayBufferNode[j].pNext            = NULL;
                                break;
                            }
                        }
                        if(j == MAX_VIDEO_BUFFER_NUM)
                        {
                            loge(" display buffer node is not enought!");
                            abort();
                        }
                        logv("displayBufferNode: j = %d",j);
                        if(p->pDisplayerBufferListHead != NULL)
                        {
                            VDisplayBufferNodeT* pNewNode = NULL;
                            pNewNode = p->pDisplayerBufferListHead;
                            
                            while(pNewNode->pNext != NULL)
                                pNewNode = pNewNode->pNext;

                            pNewNode->pNext = &p->displayBufferNode[j];
                                
                        }
                        else
                        {
                            p->pDisplayerBufferListHead = &p->displayBufferNode[j];
                        }
                        
                        break;
                    }
                }

                if(i == MAX_VIDEO_BUFFER_NUM)
                {
                    loge(" the video buffer is full, should not run here!");
                    abort();
                }
            }
            else
            {
                //* check whether return the second stream buffer
                for(i = 0; i < MAX_VIDEO_BUFFER_NUM; i++)
                {
                    unsigned int nSecondPhyBufAddr = p->mVideoBufferInfo[i].mVideoPicture.phyYBufAddr + 
                                                     p->nVideoBufferWidth*p->nVideoBufferHeight;

                    if(p->mVideoBufferInfo[i].bUsedFlag == 1
                        && p->mVideoBufferInfo[i].mVideoPicture.nBufId == pVideoPicture->nBufId
                        && nSecondPhyBufAddr == pVideoPicture->phyYBufAddr)
                    {
                        for(j = 0; j < MAX_VIDEO_BUFFER_NUM; j++)
                        {
                            if(p->displayBufferNode[j].bUseFlag == 0)
                            {
                                p->displayBufferNode[j].bUseFlag         = 1;
                                p->displayBufferNode[j].pVideoBufferInfo = &p->mVideoBufferInfo[i];
                                p->displayBufferNode[j].pNext            = NULL;
                                break;
                            }
                        }
                        if(j == MAX_VIDEO_BUFFER_NUM)
                        {
                            loge(" display buffer node is not enought!");
                            abort();
                        }
                        logv("displayBufferNode: j = %d",j);
                        if(p->pDisplayerBufferListHead != NULL)
                        {
                            VDisplayBufferNodeT* pNewNode = NULL;
                            pNewNode = p->pDisplayerBufferListHead;
                            
                            while(pNewNode->pNext != NULL)
                                pNewNode = pNewNode->pNext;

                            pNewNode->pNext = &p->displayBufferNode[j];
                                
                        }
                        else
                        {
                            p->pDisplayerBufferListHead = &p->displayBufferNode[j];
                        }
                        break;
                    }
                }

                //* store the first stream bufffer
                if(i == MAX_VIDEO_BUFFER_NUM)
                {
                    for(i = 0; i < MAX_VIDEO_BUFFER_NUM; i++)
                    {
                        if(p->mVideoBufferInfo[i].bUsedFlag == 0)
                        {
                            memcpy(&p->mVideoBufferInfo[i].mVideoPicture,pVideoPicture,sizeof(VideoPicture));
                            p->mVideoBufferInfo[i].bUsedFlag      = 1;
                            p->mVideoBufferInfo[i].bDisplayedFlag = 0;
                            p->mVideoBufferInfo[i].bCanReturnFlag = 0;
                            
                            logv("displayer buffer sucess : i = %d, phy = %x, id = %d, pts = %lld",
                                  i,
                                  p->mVideoBufferInfo[i].mVideoPicture.phyYBufAddr,
                                  p->mVideoBufferInfo[i].mVideoPicture.nBufId,
                                  p->mVideoBufferInfo[i].mVideoPicture.nPts);
                            
                            break;
                        }
                    }

                    if(i == MAX_VIDEO_BUFFER_NUM)
                    {
                        loge(" the video buffer is full, should not run here!");
                        abort();
                    }
                }
            }
            
            pthread_mutex_unlock(&p->videoBufferInfoMutex);
            return 0;
        }
        case VIDEO_RENDER_RETURN_BUFFER:
        {
            
            pthread_mutex_lock(&p->videoBufferInfoMutex);
            
            VideoPicture* pVideoPicture = (VideoPicture*)param;
            logv("callback --- return buffer: phy = %x, id = %d, pts = %lld",
                  pVideoPicture->phyYBufAddr,
                  pVideoPicture->nBufId,
                  pVideoPicture->nPts);
            
            for(i = 0; i < MAX_VIDEO_BUFFER_NUM; i++)
            {
                unsigned int nSecondPhyBufAddr = p->mVideoBufferInfo[i].mVideoPicture.phyYBufAddr + 
                                                 p->nVideoBufferWidth*p->nVideoBufferHeight;
                
                if(p->mVideoBufferInfo[i].bUsedFlag == 1
                   && p->mVideoBufferInfo[i].mVideoPicture.nBufId == pVideoPicture->nBufId
                   &&(p->mVideoBufferInfo[i].mVideoPicture.phyYBufAddr == pVideoPicture->phyYBufAddr
                      || nSecondPhyBufAddr == pVideoPicture->phyYBufAddr
                      || p->mVideoBufferInfo[i].mVideoPicture.pData0 == pVideoPicture->pData0))
                {
                    if(p->bVideoWithTwoStream == 1)
                    {
                        p->mVideoBufferInfo[i].nReturnCount++;
                        if(p->mVideoBufferInfo[i].nReturnCount == 2)
                            p->mVideoBufferInfo[i].bCanReturnFlag = 1;    
                    }
                    else
                        p->mVideoBufferInfo[i].bCanReturnFlag = 1;
                    break;
                }
            }

            pthread_mutex_unlock(&p->videoBufferInfoMutex);
            
            if(i == MAX_VIDEO_BUFFER_NUM)
            {
                logw("the return buffer had not been used, we cancel it to gpu!");
                pthread_mutex_lock(&p->layerCtrlMutex);
                LayerQueueBuffer(p->pLayerCtrl, pVideoPicture, 0);
                LayerReturnBuffer(p->pLayerCtrl,pVideoPicture);
                p->nBufferOwnedByDecoderNum--;
                pthread_mutex_unlock(&p->layerCtrlMutex);
                //abort();
            }
            return 0;
        }
        case VIDEO_RENDER_RESOLUTION_CHANGE:
        {
            p->bResolutionChange = (int)param;
            return 0;
        }
        default:
        {
            loge("the message is not support : eMessageId = %d",eMessageId);
            return -1;
        }
    }
    return 0;
}