/*
 * Copyright (C) 2010 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef A_VORBIS_ASSEMBLER_H_

#define A_VORBIS_ASSEMBLER_H_

#include "ARTPAssembler.h"

#include <media/stagefright/foundation/AString.h>

#include <utils/List.h>
#include <utils/RefBase.h>


namespace android {

struct ABuffer;
struct AMessage;

struct AVorbisAssembler : public ARTPAssembler {
    AVorbisAssembler(
            const sp<AMessage> &notify, const AString &desc,
            const AString &params);

protected:
    virtual ~AVorbisAssembler();

    virtual AssemblyStatus assembleMore(const sp<ARTPSource> &source);
    virtual void onByeReceived();
    virtual void packetLost();

private:
    enum FragmentType {
    	//don't touch the value.
    	NotFragmented 		 = 0,
    	StartFragment 		 = 1,
    	ContinuationFragment = 2,
    	EndFragment			 = 3,
    };

    enum VorbisDataType {
    	//don't touch the value.
    	RawVorbisPayload 			= 0,
    	PackedConfigurationPayload	= 1,
    	LegacyCommentPayload		= 2,
    	VorbisDataTypeReserved		= 3,
    };

    sp<AMessage> mNotifyMsg;
    AString mParams;
    uint32_t mAccessUnitRTPTime;
    bool mNextExpectedSeqNoValid;
    uint32_t mNextExpectedSeqNo;
    uint32_t mCurrentIdent;
    bool mAccessUnitDamaged;
    List<sp<ABuffer> > mPackets;
    List<sp<ABuffer> > mConfigs;
    bool mAddPackedHeader;
    bool mSubmitConfiguration;

    uint32_t mPageNum;
    uint8_t mHeaderType;

    bool isAdjustLengthNeccessary(const sp<ABuffer> &buffer);
    int32_t getPacketLength(const sp<ABuffer> &buffer);
    int32_t splitXiphHeaders(const sp<ABuffer> &codecData, sp<ABuffer> *configuration);
    sp<ABuffer> getPackedHeaders(uint32_t ident);
    sp<ABuffer> generatePageHeader(uint32_t len);
    void parsePackedHeaders(const sp<ABuffer> &headers);
    void parsePackedConfiguration(const sp<ABuffer> &buffer, sp<ABuffer> &accessUnit);
    void parseRawPayload(const sp<ABuffer> &buffer, sp<ABuffer> &accessUnit);
    void parseBufferInfo(const sp<ABuffer> &buffer, sp<ABuffer> &accessUnit);
    AssemblyStatus addPacket(const sp<ARTPSource> &source);
    void submitAccessUnit();

    DISALLOW_EVIL_CONSTRUCTORS(AVorbisAssembler);
};

}  // namespace android

#endif  // A_VORBIS_ASSEMBLER_H_
