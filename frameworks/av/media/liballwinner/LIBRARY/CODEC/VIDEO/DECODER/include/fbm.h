
#ifndef FBM_H
#define FBM_H

#include "vdecoder.h"
#include <pthread.h>

enum BUFFER_TYPE
{
    BUF_TYPE_REFERENCE_DISP = 0,
    BUF_TYPE_ONLY_REFERENCE,
    BUF_TYPE_ONLY_DISP,
};

typedef struct FRAMENODE FrameNode;
struct FRAMENODE
{
    int          bUsedByDecoder;
    int          bUsedByRender;
    int          bInValidPictureQueue;
    int          bAlreadyDisplayed;
    VideoPicture vpicture;
    FrameNode*   pNext;
};


typedef struct FRAMEBUFFERMANAGER
{
    pthread_mutex_t mutex;
    int             nMaxFrameNum;
    int             nEmptyBufferNum;
    int             nValidPictureNum;
    FrameNode*      pEmptyBufferQueue;
    FrameNode*      pValidPictureQueue;
    int             bThumbnailMode;
    FrameNode*      pFrames;

    Callback        callback;
    void*           pUserData;
    int             bUseGpuBuf;
    int             nAlignValue;
}Fbm;

typedef struct FBMCREATEINFO
{
    int nFrameNum; 
    int nWidth;
    int nHeight;
    int ePixelFormat; 
    int bThumbnailMode;
    Callback callback;
    void* pUserData;
    int bGpuBufValid;
    int nAlignStride;
    int nBufferType;
    int bProgressiveFlag;
    int bIsSoftDecoderFlag;
}FbmCreateInfo;


Fbm* FbmCreate(FbmCreateInfo* pFbmCreateInfo);

void FbmDestroy(Fbm* pFbm);

void FbmFlush(Fbm* pFbm);

int FbmGetBufferInfo(Fbm* pFbm, VideoPicture* pVPicture);

int FbmTotalBufferNum(Fbm* pFbm);

int FbmEmptyBufferNum(Fbm* pFbm);

int FbmValidPictureNum(Fbm* pFbm);

VideoPicture* FbmRequestBuffer(Fbm* pFbm);

void FbmReturnBuffer(Fbm* pFbm, VideoPicture* pVPicture, int bValidPicture);

void FbmShareBuffer(Fbm* pFbm, VideoPicture* pVPicture);

VideoPicture* FbmRequestPicture(Fbm* pFbm);

int FbmReturnPicture(Fbm* pFbm, VideoPicture* pPicture);

VideoPicture* FbmNextPictureInfo(Fbm* pFbm);


int FbmAllocatePictureBuffer(VideoPicture* pPicture, int* nAlignValue, int nWidth, int nHeight);

int FbmFreePictureBuffer(VideoPicture* pPicture);
int FbmGetAlignValue(Fbm* pFbm);

#endif

