#ifndef __SWLOG_H__
#define __SWLOG_H__

/* logging level */
#define	LOG_LEVEL_ALL		0x00	//输出所有日志
#define	LOG_LEVEL_DEBUG		0x01	//指出细粒度信息事件对调试应用程序是非常有帮助的
#define	LOG_LEVEL_INFO		0x02	//消息在粗粒度级别上突出强调应用程序的运行过程
#define	LOG_LEVEL_WARN		0x03	//表明会出现潜在错误的情形
#define	LOG_LEVEL_ERROR		0x04	//指出虽然发生错误事件，但仍然不影响系统的继续运行
#define	LOG_LEVEL_FATAL		0x05	//指出每个严重的错误事件将会导致应用程序的退出
#define	LOG_LEVEL_OFF		0x06	//关闭日志输出

#ifdef __cplusplus
extern "C"
{
#endif
	
int sw_log_init( int level, char* targets,char* mods);
void sw_log_exit();

int sw_log_set_level( int level );
int sw_log_get_level();

/** 
 * @brief Output the logging info.
 * 
 * @param level int, the level of the logging info
 * @param mod  char*,the modduls name
 * @param file  char*,the file name
 * @param line  int,the line in the file
 * @param format char*, format string.
 * @param ... 
 * 
 * @return int , the logging level has been set previously.
 */
int sw_log( int level,const char* mod,const char* file,int line,const char *format, ... );


#ifdef __cplusplus
}
#endif

#endif /* __SWLOG_H__ */
